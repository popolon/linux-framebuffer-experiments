#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sysexits.h>
#include <time.h>
#include <math.h>
#include <signal.h>
#include <termios.h>
#include <linux/keyboard.h>
#include <linux/kd.h>

int error;
int debug = 0;
static volatile int loop = 1; // used by interrupt, must be volatile for // process usage
uint32_t framet;
struct timespec t_s;
uint64_t realt;

// Framebuffer attributes
struct fb_fix_screeninfo fb_finfo;
struct fb_var_screeninfo fb_vinfo_back, fb_vinfo;
const char *fbPath;
int fb;

uint32_t *buf;       // memmapped buffer
uint32_t *realbuffer = NULL;  // screen resolution scaled double buffer
uint8_t  *scrbuffer = NULL; // game resolution buffer byte can be 8 bits/pixel

#define DRAWBUF realbuffer

// spritesback buffer (to not have to redraw the whole buffer at each frame)
// would be useless with hw sprites or bitmaps
uint32_t *ballbuff;  // ball backbuffer (to not redraw the whole screen each frame)
uint16_t ppx = 0, // up-left corner
	 ppy = 0,
	 psx = 0, // width
	 psy = 0; // height

uint8_t sprite1[] = {
  1,1,1,1,1,1,1,1,
  1,0,0,0,0,0,0,1,
  1,0,1,0,0,1,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,1,1,0,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,0,0,0,0,1,
  1,1,1,1,1,1,1,1
};

uint8_t sprite2[] = {
  0,0,1,1,1,1,0,0,
  0,1,0,0,0,0,1,0,
  1,0,1,0,0,1,0,1,
  1,0,0,0,0,0,0,1,
  1,0,0,1,1,0,0,1,
  1,0,0,1,1,0,0,1,
  0,1,0,0,0,0,1,0,
  0,0,1,1,1,1,0,0
};



// For lowlevel keyboard rawmode input
// using event could be a more general solution for keyboard/joystick/gamepad/tablet/touchpad/...
// back is used to restore previous state at exit
struct termios termattr_back, termattr;

// Signal handler 
void int_handler(int int_sig) {
    signal(int_sig, SIG_IGN);
    loop = 0;
}

// input events
int kb;

void fbinfos_print() {
  printf("--------------------------------------------------------\n");
  printf("Infos from buffers strucs (see linux/fb.h for more info)\n");
  printf("--------------------------------------------------------\n");
  printf("fix frame buffer info\n--------\n");

  printf("startmem: %u (0x%016X) length: %d mmio_start: %d mmio_length: %d line_length: %d (%d %dbits pixels)\n", fb_finfo.smem_start, fb_finfo.smem_start, fb_finfo.smem_len, fb_finfo.mmio_start, fb_finfo.mmio_len, fb_finfo.line_length, fb_finfo.line_length/(fb_vinfo.bits_per_pixel/8), fb_vinfo.bits_per_pixel);
  printf("type: %d visual: %d\n", fb_finfo.type, fb_finfo.visual);
  printf("xpanstep: %d ypanstep: %d ywrapstep: %d\n", fb_finfo.xpanstep, fb_finfo.ypanstep, fb_finfo.ywrapstep );
  printf("accel: %d\n",fb_finfo.accel);
  printf("capabilities: %d\n", fb_finfo.capabilities);
  printf("\nvar info\n--------\n");

  printf("xres/yres: %d x %d\n", fb_vinfo.xres, fb_vinfo.yres);
  printf("xres/yres (virtual): %d x %d\n", fb_vinfo.xres_virtual, fb_vinfo.yres_virtual);
  printf("bits per pixel %d\n", fb_vinfo.bits_per_pixel);
  printf("RGBA lengths: %d %d %d %d  RGBA offsets: %d %d %d %d\n", fb_vinfo.red.length, fb_vinfo.green.length, fb_vinfo.blue.length, fb_vinfo.transp.length
		, fb_vinfo.red.offset, fb_vinfo.green.offset, fb_vinfo.blue.offset, fb_vinfo.transp.offset);
  printf("rotation: %d\n", fb_vinfo.rotate);
  if (fb_vinfo.grayscale <= 1)
    printf("grayscale (0:color 1:gray >1: FOURCC: %d\n", fb_vinfo.grayscale);
  else
    printf("grayscale (0:color 1:gray >1: FOURCC: %04X\n", fb_vinfo.grayscale);

  printf("sync: %d, left_margin: %d right_margin: %d upper_margin: %d lower_margin: %d\n", fb_vinfo.sync, fb_vinfo.left_margin, fb_vinfo.right_margin, fb_vinfo.upper_margin, fb_vinfo.lower_margin);
  printf("--------------------------------------------------------\n");
  printf("/proc/fb: ");
  FILE *procfd = fopen("/proc/fb", "r");
  char buffer[256];
  if (procfd == NULL) err(EX_OSFILE, "/proc/fd");
  else {
    fgets(buffer, sizeof(buffer), procfd);
    printf("%s", buffer);
    fclose(procfd);
  }
  printf("--------------------------------------------------------\n");
}

int inits() {
  // initialise interruption handler
  signal(SIGINT, int_handler);

  fbPath = getenv("FRAMEBUFFER");
  if (!fbPath) fbPath = "/dev/fb0";

  fb = open(fbPath, O_RDWR);
  if (fb < 0) err(EX_OSFILE, "%s", fbPath);

  ballbuff = (uint32_t *) malloc(1024*1024 * sizeof (uint32_t));

  // Get fixed screen information
  error = ioctl(fb, FBIOGET_FSCREENINFO, &fb_finfo);
  if (error) err(EX_IOERR, "FBIOGET_FSCREENINFO: %s", fbPath);

  error = ioctl(fb, FBIOGET_VSCREENINFO, &fb_vinfo);
  if (error) err(EX_IOERR, "FBIOGET_VSCREENINFO: %s", fbPath);

//  /* Allocate screen to draw to at game resolution (trick got from fbDoom) */
//  scrbuffer = (uint8_t*) malloc(SCREENWIDTH * SCREENHEIGHT);  // For DOOM to draw on
  realbuffer = (uint32_t*) malloc(fb_vinfo.xres * fb_vinfo.yres * (fb_vinfo.bits_per_pixel/8)); // Used for a single write() syscall toward fbdev

  fbinfos_print();

  size_t size =  4 * fb_vinfo.xres * fb_vinfo.yres;
  buf = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);
  if (buf == MAP_FAILED) err(EX_IOERR, "Create buffer: %s", fbPath);

//  keyboard_init();
}

int bg_fun_and_simple_color_gradiant() {
  for (uint16_t j=0; j<fb_vinfo.yres; j++)
   for (uint16_t i=0; i<fb_vinfo.xres; i++)
    //DRAWBUF[j*fb_vinfo.xres + i] = i*256/7.5;
    DRAWBUF[j*fb_vinfo.xres + i] = i/7.5;
}

int bg_color_gradiant() {
  uint64_t y = 0;
  for (uint16_t j=0; j<(fb_vinfo.yres>>2); j++) {
    uint8_t shift = (uint8_t) 128 + 128*sin(realt/200.0) *sin( realt/100.0 + j * 6.28 / (fb_vinfo.yres>>2));
    for (uint16_t i=0; i<fb_vinfo.xres; i++) {
      //DRAWBUF[j*fb_vinfo.xres + i] = i*256/7.5;
      DRAWBUF[y + i] = (((i+shift) & 0xFF)<<16) + (((i+shift -128) & 0xFF)<<8) + ((i+shift -64 + (uint8_t) floor(realt/50.0)) & 0xFF);
    }
    y += fb_vinfo.xres;
  }
  uint64_t quart = (fb_vinfo.xres * fb_vinfo.yres)>>1;
  memcpy(DRAWBUF + ((fb_vinfo.yres>>2) * fb_vinfo.xres), DRAWBUF, quart<<2);
  memcpy(DRAWBUF + ((fb_vinfo.yres>>1) * fb_vinfo.xres), DRAWBUF, (quart<<2)*2);
}

int bg_bw_gradiant() {
  uint64_t y = fb_vinfo.xres;
  for (uint16_t j=0; j<fb_vinfo.yres; j++)
   y += fb_vinfo.xres;
   for (uint16_t i=0; i<fb_vinfo.xres; i++) {
     uint8_t comp = (uint8_t) i;
     DRAWBUF[y + i] = (comp <<16) + (comp <<8) + comp;
   }
}

int releases() {
 // keyboard_release();

  fflush(stdout);
  //close(kb);

  // free other buffers
  printf("free scrbuffer\n");
  free(scrbuffer);
  printf("free realbuffer\n");
  free(realbuffer);
  printf("free ballbuff\n");
  free(ballbuff);
  printf("releases() ended\n");
}

int read_buff(uint16_t x, uint16_t y, uint16_t sx, uint16_t sy){
  ppx = x; // up-left corner
  ppy = y;
  psx = sx; // width/height
  psy = sy;
  if ( psx != 0 && psy != 0 )
    for (uint16_t j = 0 ; j < psy; j++)
      for (uint16_t i = 0 ; i < psx; i++)
        ballbuff[j * psy + i] = DRAWBUF[(ppy + j) * fb_vinfo.xres + ppx + i];
  return 0;
}


int rect(uint16_t x, uint16_t y, uint16_t sx, uint16_t sy, uint32_t color) {
  if (sx != 0 && sy != 0) {
    uint64_t p1 = y * fb_vinfo.xres + x;
 
    for (uint16_t j = 0 ; j < sy; j++) {
      for (uint16_t i = 0 ; i < sx ; i++)
        DRAWBUF[p1 + i] = color;
      p1 += fb_vinfo.xres;
    }
  }
}

int rectb(uint16_t x, uint16_t y, uint16_t sx, uint16_t sy, uint32_t color) {
  if (sx != 0 && sy != 0) {
    uint64_t p1 = y * fb_vinfo.xres + x;
        
    for (uint16_t j = 0 ; j < sy; j++) {
      DRAWBUF[p1] = color; // left line
      DRAWBUF[p1 + sx - 1] = color; //right line
      p1 += fb_vinfo.xres;
    }
    p1 = y * fb_vinfo.xres + x;
    uint64_t p2 = p1 + (sy - 1) * fb_vinfo.xres;
    for (uint16_t i = 1 ; i < sx -1; i++) {
      DRAWBUF[p1++] = color; // up line
      DRAWBUF[p2++] = color; // bottom line
    }
  }
}

int buff_debug_square() {
  if (debug && psx != 0 && psy != 0)
    rectb(ppx, ppy, psx, psy, 0xFF0000);
}

int write_buff() {
  if ( psx != 0 && psy != 0 )
    for (uint16_t j = 0 ; j < psy; j++)
      for (uint16_t i = 0 ; i < psx; i++)
        DRAWBUF[(ppy + j) * fb_vinfo.xres + ppx + i ] = ballbuff[j * psy + i];
  return 0;
}

int buff_to_screen() {
  lseek(fb, 0, SEEK_SET);
  write(fb,DRAWBUF, fb_vinfo.xres * fb_vinfo.yres * (fb_vinfo.bits_per_pixel/8));
}

int hypno_circle(int16_t cx, int16_t cy, uint16_t br) {
//    write_buff();
//    read_buff(cx-br , cy-br, br*2+1,br*2+1);
    uint32_t bottom = 64;
    for (uint32_t y = cy - br; y < cy + br; y++) {
      for (uint32_t x = cx - br ; x < cx + br; x++) {
        float R2 = (x-cx)*(x-cx) + (y-cy)*(y-cy);
        float R = sqrt(R2);
        if ( R2 < br*br ) {
          uint32_t bit = (uint32_t) 0x777777 +
                  ((uint8_t) floor(0x77 * sin((R - framet) / 32.0))<<16) + // R
                  ((uint8_t) floor(0x77 * cos((R+16)/32.0))<<8) + // G
                  ((uint8_t) floor(0x77 * sin((R+32)/32.0)));  // B
          DRAWBUF[y * fb_vinfo.xres + x] = bit;
        }
      }
    }
    buff_debug_square();
}

int draw_sprite1( uint8_t *sprite, uint16_t x0, uint16_t y0, uint8_t factor, uint32_t color) {
  uint64_t y = y0 * fb_vinfo.xres + x0;
  uint16_t line = 0;
  if (factor == 0) return 0;
  for (uint8_t j=0; j<8; j++) {
    for (uint16_t i=0; i<8; i++)
      if ( sprite[line+i]) {
	switch (factor) {
	  case 1:
            DRAWBUF[y+i] = color;
            break;
	  case 2:
	    DRAWBUF[y + i*2 - 1] = color;
	    DRAWBUF[y + i*2] = color;
	    DRAWBUF[y + fb_vinfo.xres + i*2 - 1] = color;
	    DRAWBUF[y + fb_vinfo.xres + i*2] = color;
 	    break; 
	}
      }
    line += 8;
    y += fb_vinfo.xres;
    if ( factor == 2 ) { y += fb_vinfo.xres; }
  }
}

// ##################################
int main() {
  inits();

  framet = 0;
  uint32_t ball_ray = fb_vinfo.xres >>4;
  uint32_t Cx = fb_vinfo.xres >>1,
           Cy = fb_vinfo.yres >>1;
    // bg_color_gradiant();
    bg_color_gradiant();

  while(loop) {
    //uint32_t br = ball_ray + ball_ray*cos(framet/32.0);
    uint32_t br = ball_ray;

    // time stuff
    framet += 1;
    clock_gettime(CLOCK_REALTIME, &t_s); // real time in second + nanoseconds
    realt = (t_s.tv_sec % 2678400) * 1000 + (t_s.tv_nsec / 1000000); // convert to microsecond 64b number with 31j gap

    bg_color_gradiant();
    // Set the position of the ball
//    int cx = (int) Cx + (fb_vinfo.xres >>2) * cos( framet / 16.0);
//    int cy = (int) Cy + (fb_vinfo.yres >>2) * sin( framet / 16.0);
    int cx = (int) Cx + (Cx-br) * cos( framet / 16.0);
    int cy = (int) Cy + (Cy-br) * sin( framet / 16.0);

    hypno_circle(cx,cy,br);

    rect(100,100,80,10,0x0000FF);
    rect(500,500,10,80,0x00FF00);
    draw_sprite1( sprite1, 20,20, 2, 0xFFFF00);
    draw_sprite1( sprite2, 36,20, 2, 0x0000FF);


    if (debug) // display changing color line every 1/2 s if 50fps
      if (framet%50 <25) for (int i=0;i<512;i++) DRAWBUF[i] = 0xFFFFFF;
      else for (int i=0;i<512;i++) DRAWBUF[i] = 0xFF0000;
		
    // wait for vsync
    int zero = 0;
    ioctl(fb, FBIO_WAITFORVSYNC, &zero);

    // then copy back buffer to framebuffer
    buff_to_screen();
  }
  printf("break\n");
  releases();
}
