CC = gcc
LIBS = -lm

files := ball_clean sprites

all: ${files}

ball_clean: ball_clean.c
	${CC} -o $@ $? ${LIBS}

sprites: sprites.c
	${CC} -o $@ $? ${LIBS}

clean:
	rm ${files}
